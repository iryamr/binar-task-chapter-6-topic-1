package id.binar.chapter6.smsreceiver

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.telephony.SmsMessage
import android.widget.Toast

class SmsReceiver : BroadcastReceiver() {

    @SuppressLint("ObsoleteSdkInt")
    override fun onReceive(context: Context, intent: Intent) {
        val extras = intent.extras
        val sms = extras?.get("pdus") as Array<*>
        for (item in sms.indices) {
            val smsMessage = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val format = extras.getString("format")
                SmsMessage.createFromPdu(sms[item] as ByteArray, format)
            } else {
                SmsMessage.createFromPdu(sms[item] as ByteArray)
            }

            val phoneNumber = smsMessage.originatingAddress
            val messageText = smsMessage.messageBody.toString()

            Toast.makeText(
                context, """
                Phone Number: $phoneNumber
                Message: $messageText
            """.trimIndent(),
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}